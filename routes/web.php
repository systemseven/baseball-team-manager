<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/',function(){
   echo 'what to do with the homepage';
});

Route::get('/login', function () {
    return view('pages.login');
});

Route::get('/me','\App\Http\Controllers\MeController@index');
