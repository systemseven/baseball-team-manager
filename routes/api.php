<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::group(['prefix' => 'v1'], function () {
    Route::post('authenticate', '\App\Http\Controllers\Api\AuthenticateController@authenticate');
    Route::get('authenticate/user', '\App\Http\Controllers\Api\AuthenticateController@authenticatedUser');
    Route::post('logout', '\App\Http\Controllers\Api\AuthenticateController@logout');
    Route::get('token', '\App\Http\Controllers\Api\AuthenticateController@getToken');
});

Route::group(['prefix' => 'v1', 'middleware'=>'jwt.auth'], function () {
#Route::group(['prefix' => 'v1'], function () {
    Route::resource('users','Api\UserController');
    Route::resource('users/{user}/players','Api\UserPlayerController');
    Route::resource('teams','Api\TeamController');
    Route::resource('teams/{team}/players','Api\TeamPlayerController', ['only'=>'index']);
    Route::resource('teams/{team}/games','Api\TeamGameController');
    //catch all route for the root URL.
    //will also be used in testing that middleware is running
    Route::any('/', function () {
        $response = [
            'errors'=>[
                [
                    'status' => 404,
                    'source' => [ 'pointer' => url()->current() ],
                    'title' =>  'Nothing Here',
                    'detail' => "There is no data at this location"
                ]
            ]
        ];
        return response()->json($response,404);
    });
});

