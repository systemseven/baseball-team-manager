<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'cellphone'=> $faker->phoneNumber,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Player::class, function (Faker\Generator $faker) {
    $image = $faker->unique()->numberBetween(1,99);
    return [
        'user_id' => $faker->numberBetween(1,60),
        'team_id' => $faker->numberBetween(1,12),
        'firstname' => $faker->firstNameMale,
        'lastname' => $faker->lastName,
        'photo'=> "https://randomuser.me/api/portraits/men/${image}.jpg",
        'jersey' => $faker->unique()->numberBetween(1,45),
        'bats' => $faker->randomElement(['R','L']),
        'fields'=>$faker->randomElement(['R','L']),
        'dob'=>$faker->dateTimeBetween('-10 years', '-9 years'),
    ];
});

$factory->define(App\Team::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->randomElement(['Yankees','Cubs','Dodgers','Pirates','White Sox','Mets','Red Sox','Angels','Cardinals','Giants','Phillies','Nationals','Braves','Marlins','Rangers','Reds','Indians','Tigers','Blue Jays','Athletics','Astors']),
        'logo' => $faker->ean13.'.jpg'
    ];
});

$factory->define(App\Game::class, function (Faker\Generator $faker) {
    return [
        'home_team_id'=>$faker->numberBetween(1,12),
        'away_team_id'=>$faker->numberBetween(1,12),
        'field_id'=>$faker->numberBetween(1,5),
        'game_at'=>$faker->dateTimeBetween('+4 months', '+6 months'),
    ];
});
