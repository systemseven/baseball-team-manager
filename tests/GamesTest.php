<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GamesTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     *
     * Test: GET /api/v1/teams/1/games
     */
    public function it_fetches_games()
    {
        $this->seed();

        $this->get('/api/v1/teams/1/games', $this->headers($this->makeUser()));

        $this->assertResponseStatus(200);
    }


    /**
     * @test
     *
     * Test: GET /api/v1/teams/1/games/1
     */
    public function it_sees_a_game()
    {
        $team = factory(App\Team::class)->create();
        $game = factory(App\Game::class)->create(['home_team_id'=>$team->id, 'away_team_id'=>$team->id]);
        $path = "/api/v1/teams/".$team->id."/games/".$game->id;
        $this->get($path, $this->headers($this->makeUser()));
        $this->assertResponseStatus(200);
    }
}
