<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TeamsTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     *
     * Test: GET /api/v1/teams/X/players
     */
    public function it_gets_a_teams_players()
    {
        $this->seed();
        $this->get('/api/v1/teams/1/players',$this->headers($this->makeUser()));

        $this->assertResponseOk();
    }

    /**
     * @test
     *
     * Test: GET /api/v1/teams/
     */
    public function it_gets_teams()
    {
        $this->seed();
        $this->get('/api/v1/teams',$this->headers($this->makeUser()));

        $this->assertResponseOk();
    }

    /**
     * @test
     *
     * Test: GET /api/v1/teams/1
     */
    public function it_shows_a_single_team()
    {
        $this->seed();
        $this->get('/api/v1/teams/1',$this->headers($this->makeUser()));

        $this->assertResponseOk();
    }

    /**
     * @test
     *
     * Test: GET /api/v1/teams/99999
     */
    public function it_404s_when_a_team_doesnt_exist()
    {
        $this->seed();
        $this->get('/api/v1/teams/99999',$this->headers($this->makeUser()));

        $this->assertResponseStatus(404);
    }

    /**
     * @test
     *
     * Test: POST /api/v1/teams/
     */
    public function it_creates_teams()
    {
        $newTeam = ['name' => 'Braves', 'logo' => 'filename.jpg'];
        $this->post('/api/v1/teams', $newTeam, $this->headers($this->makeUser()))
            ->seeStatusCode(201);
    }

    /**
     * @test
     *
     * Test: POST /api/v1/teams/
     */
    public function it_creates_teams_only_authorized()
    {
        $newTeam = ['name' => 'Braves', 'logo' => 'filename.jpg'];
        $this->post('/api/v1/teams', $newTeam)
            ->seeStatusCode(400);
    }


}
