<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UsersTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     *
     * Test: GET /api/v1/users
     */
    public function it_fetches_users()
    {
        $user = $this->makeUser();

        $this->seed();

        $this->get('/api/v1/users', $this->headers($user));

        $this->assertResponseStatus(200);
    }


    /**
     * @test
     *
     * Test: GET /api/v1/users/1
     */
    public function it_sees_a_user()
    {
        $user = $this->makeUser();

        $this->seed();

        $this->get('/api/v1/users/1', $this->headers($user));

        $this->assertResponseStatus(200);
    }

    /**
     * @test
     *
     * Test: POST /api/v1/users.
     */
    public function it_can_create_a_user()
    {
        //here we set up the dummy 'admin style' user that will use the route to create another user
        $user = $this->makeUser();

        //this is the user we will try and create
        $newUser = ['firstname' => 'John', 'lastname' => 'Doe', 'cellphone' => 7045551212, 'email' => 'test@test.com', 'password'=>bcrypt('secret')];

        $this->post('/api/v1/users', $newUser, $this->headers($user))
            ->seeStatusCode(201);
    }

    /**
     * @test
     *
     * Test: POST /api/v1/users.
     */
    public function it_can_only_create_when_authorized()
    {
        //this is the user we will try and create
        $newUser = ['firstname' => 'John', 'lastname' => 'Doe', 'cellphone' => 7045551212, 'email' => 'test@test.com', 'password'=>bcrypt('secret')];

        $this->post('/api/v1/users', $newUser)
            ->seeStatusCode(400);
    }

    /**
     * @test
     *
     * Test: GET /api/v1/users/9999999
     */
    public function it_sees_a_404(){
        $user = $this->makeUser();

        $this->seed();
        $this->get('/api/v1/users/999999',$this->headers($user));
        $this->assertResponseStatus(404);
    }

    /**
     * @test
     *
     * Test: GET /api/v1/users/1
     * Makes sure that when a middleware is in place, no data is returned
     */
    public function it_sees_a_400(){
        $this->seed();
        $this->get('/api/v1/users/1');
        $this->assertResponseStatus(400);
    }



}
