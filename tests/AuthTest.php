<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AuthTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     *
     * Test: POST /api/v1/authenticate
     */
    public function it_authenticates_a_user()
    {
        $user = factory(App\User::class)->create(['password' => bcrypt('secret')]);

        $this->post('/api/v1/authenticate', ['email' => $user->email, 'password' => 'secret'])
            ->seeJsonStructure(['token']);
    }

    /**
     * @test
     *
     * Test: POST /api/v1/authenticate
     * To make sure that I don't push the routes file with middleware commented out to github/prod
     */
    public function it_has_middleware_running()
    {
        $this->get('/api/v1')
            ->assertResponseStatus(400);
    }


}
