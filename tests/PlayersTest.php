<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PlayersTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     *
     * Test: GET /api/v1/users/1/players
     */
    public function it_fetches_players()
    {
        $user = $this->makeUser();

        $this->seed();

        $this->get('/api/v1/users/1/players', $this->headers($user));

        $this->assertResponseStatus(200);
    }


    /**
     * @test
     *
     * Test: GET /api/v1/users/1/players
     */
    public function it_sees_a_player()
    {
        $user = factory(App\User::class)->create();
        $player = factory(App\Player::class)->create();
        $player->user()->associate($user);
        $player->save();
        $path = "/api/v1/users/".$user->id."/players/".$player->id;
        $this->get($path, $this->headers($user));

        $this->assertResponseStatus(200);
    }

    /**
     * @test
     *
     * Test: POST /api/v1/users/1/players.
     */
    public function it_can_create_a_player()
    {
        $user = $this->makeUser();

        $newPlayer = ['firstname' => 'John', 'lastname' => 'Doe','user_id'=>9,'team_id'=>1];

        $this->post('/api/v1/users/1/players', $newPlayer, $this->headers($user))
            ->seeStatusCode(201);
    }

    /**
     * @test
     *
     * Test: POST /api/v1/users/1/players.
     */
    public function it_can_only_create_when_authorized()
    {
        $newPlayer = ['firstname' => 'John', 'lastname' => 'Doe','user_id'=>9,'team_id'=>1];

        $this->post('/api/v1/users/1/players', $newPlayer)
            ->seeStatusCode(400);
    }

    /**
     * @test
     *
     * Test: GET /api/v1/players/9999999
     */
    public function it_sees_a_404(){
        $user = $this->makeUser();

        $this->seed();
        $this->get('/api/v1/players/999999',$this->headers($user));
        $this->assertResponseStatus(404);
    }

    /**
     * @test
     *
     * Test: GET /api/v1/players/1
     * Makes sure that when a middleware is in place, no data is returned
     */
    public function it_sees_a_400(){
        $user = factory(App\User::class)->create();
        $player = factory(App\Player::class)->create();
        $player->user()->associate($user);
        $player->save();
        $path = "/api/v1/users/".$user->id."/players/".$player->id;

        $this->get($path);
        $this->assertResponseStatus(400);
    }

}
