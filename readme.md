### Baseball Team Manager

This application is designed to "scratch my own itch" in a variety of ways

* It will allow me to effectively communicate, schedule, and manage my 3 sons baseballs teams
* It will be a place where I can experiment with new technologies that I don't typically use in my day to day job
* It will be a public repository that I can share with potential employers

##### Testing
In order to push to git, all tests must be passing green. While I will strive for 100% coverage of the API and the front end, the goal is to make sure there is enough coverage to not introduce breaking changes

##### Branching
Until such time that this app is in use (spring 2017) all commits will be made to the master branch (once tests are 100% passing). Once the app is in production, master will remain the source of truth, and feature/defect branches will be used for further development

##### Will I open source this? 
There's a good possibility I will either open source this as a free "app" but that won't be until I have it working perfect

##### Why not a separate codebase for the API?
Typically I like to do a Lumen application for the API and a Laravel application for the customer facing piece. This app will be 2 in one.

For this particular app, since it's designed to be used only by me and to help me find another role, it makes more sense to keep the two integrated.

This may change in the future, but for now, the API and the customer facing app will be in one Laravel install.

