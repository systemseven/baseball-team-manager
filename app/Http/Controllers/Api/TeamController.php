<?php

namespace App\Http\Controllers\Api;

use App\Baseball\Traits\JSONAPITrait;
use App\Http\Requests\StoreTeamRequest;
use App\Team;
use App\Transformers\TeamTransformer;
use Cyvelnet\Laravel5Fractal\Facades\Fractal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeamController extends Controller
{
    use JSONAPITrait;


    public function index()
    {
        $teams = Team::orderBy('name','ASC')->paginate(25);
        return Fractal::collection($teams, new TeamTransformer,'team');
    }

    public function show($id)
    {
        $team = Team::find($id);
        if(!$team) {
            return $this->respondNotFound('team',$id);
        }
        return Fractal::item($team, new TeamTransformer,'team');
    }

    public function store(StoreTeamRequest $request)
    {
        $attributes = $request->only(['name','logo']);
        $team = Team::create($attributes);
        if ($team) {
            return $this->respondCreated('team',$team);
        }

        return $this->respondBadRequest();
    }

}
