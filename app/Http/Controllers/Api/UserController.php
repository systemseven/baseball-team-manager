<?php

namespace App\Http\Controllers\Api;

use App\Baseball\Traits\JSONAPITrait;
use App\Http\Requests\StoreUserRequest;
use App\Transformers\UserTransformer;
use App\User;
use Cyvelnet\Laravel5Fractal\Facades\Fractal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class UserController extends Controller
{
    use JSONAPITrait;

    public function index()
    {
        $users = User::orderBy('lastName','ASC')->orderBy('firstName','ASC')->paginate(25);
        return Fractal::collection($users, new UserTransformer,'user');
    }

    public function show($id)
    {
        $user = User::find($id);
        if(!$user) {
            return $this->respondNotFound('user',$id);
        }
        return Fractal::item($user, new UserTransformer,'user');
    }

    public function store(StoreUserRequest $request)
    {
        $attributes = $request->only(['firstname','lastname','cellphone','email','password']);
        $user = User::create($attributes);
        if ($user) {
            return $this->respondCreated('user',$user);
        }

        return $this->respondBadRequest();
    }



}
