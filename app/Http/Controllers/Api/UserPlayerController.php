<?php

namespace App\Http\Controllers\Api;

use App\Baseball\Traits\JSONAPITrait;
use App\Http\Requests\StorePlayerRequest;
use App\Player;
use App\Transformers\PlayerTransformer;
use Cyvelnet\Laravel5Fractal\Facades\Fractal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserPlayerController extends Controller
{
    use JSONAPITrait;

    public function index($user)
    {
        $players = Player::where('user_id',$user)
            ->orderBy('team_id','ASC')
            ->orderBy('lastName','ASC')
            ->orderBy('firstName','ASC')
            ->paginate(25);
        return Fractal::collection($players, new PlayerTransformer,'player');
    }

    public function show($user,$player)
    {
        $user = Player::where('user_id',$user)->where('id',$player)->first();
        if(!$user) {
            return $this->respondNotFound('player',$player);
        }
        return Fractal::item($user, new PlayerTransformer,'player');
    }

    public function store($user, StorePlayerRequest $request)
    {
        $attributes = $request->only(['firstname','lastname','user_id','team_id','photo','jersey','bats','fields','dob']);
        $player = Player::create($attributes);
        if ($player) {
            return $this->respondCreated('player',$player);
        }

        return $this->respondBadRequest();
    }

}
