<?php

namespace App\Http\Controllers\Api;

use App\Baseball\Traits\JSONAPITrait;
use App\Game;
use App\Transformers\GameTransformer;
use Cyvelnet\Laravel5Fractal\Facades\Fractal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeamGameController extends Controller
{
    use JSONAPITrait;

    public function index($team)
    {
        $players = Game::where('home_team_id',$team)
            ->orWhere('away_team_id',$team)
            ->orderBy('game_at','ASC')
            ->paginate(25);
        return Fractal::collection($players, new GameTransformer,'game');
    }

    public function show($team, $game)
    {
        $game = Game::where('id',$game)->first();
        if(!$game) {
            return $this->respondNotFound('game',$game);
        }
        return Fractal::item($game, new GameTransformer,'game');
    }

    public function store($team, StoreGameRequest $request)
    {

    }
}
