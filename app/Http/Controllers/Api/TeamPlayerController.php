<?php

namespace App\Http\Controllers\Api;

use App\Baseball\Traits\JSONAPITrait;
use App\Player;
use App\Transformers\PlayerTransformer;
use Cyvelnet\Laravel5Fractal\Facades\Fractal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeamPlayerController extends Controller
{
    use JSONAPITrait;


    public function index($team)
    {
        $players = Player::where('team_id',$team)
            ->orderBy('lastName','ASC')
            ->orderBy('firstName','ASC')
            ->paginate(25);
        return Fractal::collection($players, new PlayerTransformer,'player');
    }


}
