<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePlayerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname'      => 'required',
            'lastname'     => 'required',
            'user_id'    => 'required',
            'team_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'firstname.required' => 'First Name is required',
            'lastname.required'  => 'Last Name is required',
            'user_id.required'  => 'Parent must be provided',
            'team_id.email'  => 'Team must be provided',
        ];
    }
}
