<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname'      => 'required',
            'lastname'     => 'required',
            'email'    => 'required|email|unique:users',
            'password' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'firstname.required' => 'First Name is required',
            'lastname.required'  => 'Last Name is required',
            'email.required'  => 'Email Address is required',
            'email.email'  => 'Email Address must be a properly formatted email address',
            'email.unique'  => 'Email Address is already in use',
            'password.required'  => 'Password is required',
        ];
    }

}
