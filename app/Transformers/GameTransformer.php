<?php

namespace App\Transformers;

use App\Team;
use Carbon\Carbon;
use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class GameTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * Transform object into a generic array
     *
     * @var $resource
     * @return array
     */
    public function transform($resource)
    {
        $game_at = new Carbon($resource->game_at);
        $home_team = Team::find($resource->home_team_id);
        $away_team = Team::find($resource->away_team_id);
        return [
            'id' => $resource->id,
            'tournament'=>$resource->tournament_id,
            'home_team'=>[
                'name'=> $home_team->name,
                'logo'=> $home_team->logo,
                'score'=>$resource->home_team_score
            ],
            'away_team'=>[
                'name'=> $away_team->name,
                'logo'=> $away_team->logo,
                'score'=>$resource->away_team_score
            ],
            'game_at'=>$game_at->format('F j, Y H:i A')
        ];
    }
}
