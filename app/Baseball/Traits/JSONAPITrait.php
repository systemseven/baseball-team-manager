<?php


namespace App\Baseball\Traits;


trait JSONAPITrait
{

    protected function respondNotFound($type = "resource", $id = 0)
    {

        $response = [
            'errors'=>[
                [
                  'status' => 404,
                  'source' => [ 'pointer' => url()->current() ],
                  'title' =>  ucfirst($type).' Not Found',
                  'detail' => ($id) ? "The ${type} object with the id of ${id} was not found" : "The ${type} object was not found"
                ]
            ]
        ];
        return response()->json($response, 404);
    }

    protected function respondCreated($type = "resource",$obj)
    {
        $response = [
            'data'=>[
              'type' => $type,
              'id' => $obj->id,
              'attributes' =>  $obj
            ]
        ];

        return response()->json($response, 201);
    }

    protected function respondBadRequest()
    {

    }

/*
    public function error($errors, $status = 404)
    {
        $response = [
            'errors' => []
        ];
        foreach($errors as $error) {
            array_push($response['errors'], $error);
        }
        return response()->json($response, $status);
    }
*/
}