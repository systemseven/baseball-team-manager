<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->
    @if(!Request::is('login'))
        <script>
            var token = localStorage.getItem('api_token');
            if(token === null || token === undefined){window.location = '/login';}
        </script>
    @endif
    <link rel="stylesheet" href="{{ elixir('css/app.css') }}">

</head>
<body>
<main id="app">
    @yield('content')
</main>

<script src="{{ elixir('js/app.js') }}"></script>
<!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->

<script>
    window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;
    ga('create','UA-XXXXX-Y','auto');ga('send','pageview')
</script>
<script src="https://www.google-analytics.com/analytics.js" async defer></script>

</body>
</html>